//
//  LoginView.h
//  Cartrize
//
//  Created by Admin on 19/07/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <Social/Social.h>
#import "MBProgressHUD.h"
@interface LoginView : UIViewController<UITextFieldDelegate,MBProgressHUDDelegate>{
    IBOutlet UITextField *emailTf;
    IBOutlet UITextField *passTf;
    FBSession *session;
    NSHTTPCookie *cookie;
}
@property(nonatomic,retain)MBProgressHUD *progressHud;
@property(nonatomic,retain)UIWindow *window;
@property (nonatomic) ACAccountStore *accountStore;

-(IBAction)loginAction:(id)sender;
-(IBAction)fbLoginAction:(id)sender;
-(IBAction)twLoginAction:(id)sender;
-(IBAction)ForgotPasswordAction:(id)sender;
-(IBAction)sKipAction:(id)sender;
@end
