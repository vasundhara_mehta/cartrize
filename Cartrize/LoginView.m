//
//  LoginView.m
//  Cartrize
//
//  Created by Admin on 19/07/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "LoginView.h"
#import "iToast.h"
#import "CartrizeWebservices.h"
#import "GridViewController.h"
#import "JSON.h"
@interface LoginView ()

@end

@implementation LoginView
@synthesize accountStore,window,progressHud;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIColor *color = [UIColor whiteColor];
    emailTf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"UserID/Mobile Number" attributes:@{NSForegroundColorAttributeName: color}];
    passTf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];


    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark IBActions
-(IBAction)loginAction:(id)sender{
    emailTf.text=@"mayurwadhe@gmail.com";
    passTf.text=@"123456";
    NSDictionary *parameters = @{@"email":emailTf.text,@"password":passTf.text};
    [CartrizeWebservices PostMethodWithApiMethod:@"userLogin" Withparms:parameters WithSuccess:^(id response)
     {
         NSLog(@"Response = %@",[response JSONValue]);
         [[NSUserDefaults standardUserDefaults]setObject:[[response JSONValue] objectForKey:@"customer_id"] forKey:@"CustomerID"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         GridViewController *gridObj=[[GridViewController alloc] init];
         [self presentViewController:gridObj animated:YES completion:nil];
         
     } failure:^(NSError *error)
     {
         NSLog(@"Error =%@",[error description]);
     }];
    
    
}
-(IBAction)fbLoginAction:(id)sender{
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"user_likes",
                            @"user_birthday",
                            @"email",
                            @"publish_stream",
                            @"publish_actions",
                            @"user_photos",
                            @"friends_photos",
                            @"read_stream",
                            @"user_about_me",
                            @"user_activities",
                            @"read_friendlists",
                            @"user_friends",
                            @"offline_access",
                            nil];
    session=[[FBSession alloc] initWithAppID:@"1470358346544418" permissions:permissions urlSchemeSuffix:nil tokenCacheStrategy:nil];
    NSHTTPCookieStorage *storage=[NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    [[FBSession activeSession] handleDidBecomeActive];
    [[FBSession activeSession] closeAndClearTokenInformation];
    [FBSession setActiveSession:session];
    
    FBSessionLoginBehavior behaviour=FBSessionLoginBehaviorForcingWebView;
    [session openWithBehavior:behaviour completionHandler:^(FBSession *session1,FBSessionState status,NSError *error){
        if (error) {
            [[iToast makeText:@"Error..Please Login again"] show];
        }else{
            [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection,id result,NSError *error){
                if (!error) {
                NSLog(@"hhhh %@",result);
                }
            }];
        }
    }];
}

-(IBAction)twLoginAction:(id)sender{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) // check Twitter is configured in Settings or not
    {
        self.accountStore = [[ACAccountStore alloc] init]; // you have to retain ACAccountStore
        
        ACAccountType *twitterAcc = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        [self.accountStore
         requestAccessToAccountsWithType:twitterAcc
         options:NULL
         completion:^(BOOL granted, NSError *error) {
             NSArray *accountsArray = [accountStore accountsWithAccountType:twitterAcc];

             if (granted) {
                 ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                 NSLog(@"%@",twitterAccount.username);
                 NSLog(@"%@",twitterAccount.userFullName);

/*
                 //  Step 2:  Create a request
                 NSArray *twitterAccounts =
                 [self.accountStore accountsWithAccountType:twitterAcc];
                 NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"
                               @"/1.1/statuses/show.json"];
//                 NSDictionary *params = @{@"screen_name" : @"vasundhara1406",
//                                          @"include_rts" : @"0",
//                                          @"trim_user" : @"1",
//                                          @"count" : @"50"};
                 NSDictionary *params = @{@"screen_name" :twitterAccount.username,
                                          };

                 SLRequest *request =
                 [SLRequest requestForServiceType:SLServiceTypeTwitter
                                    requestMethod:SLRequestMethodGET
                                              URL:url
                                       parameters:params];
                 
                 //  Attach an account to the request
                 [request setAccount:[twitterAccounts lastObject]];
                 
                 //  Step 3:  Execute the request
                 [request performRequestWithHandler:
                  ^(NSData *responseData,
                    NSHTTPURLResponse *urlResponse,
                    NSError *error) {
                      
                      if (responseData) {
                          if (urlResponse.statusCode >= 200 &&
                              urlResponse.statusCode < 300) {
                              
                              NSError *jsonError;
                              NSDictionary *timelineData =
                              [NSJSONSerialization
                               JSONObjectWithData:responseData
                               options:NSJSONReadingAllowFragments error:&jsonError];
                              if (timelineData) {
                                  NSLog(@"Timeline Response: %@\n", timelineData);
                              }
                              else {
                                  // Our JSON deserialization went awry
                                  NSLog(@"JSON Error: %@", [jsonError localizedDescription]);
                              }
                          }
                          else {
                              // The server did not respond ... were we rate-limited?
                              NSLog(@"The response status code is %d",
                                    urlResponse.statusCode);
                          }
                      }
                  }];
           */
    }
         }];
        
    }
    else
    {
        [[iToast makeText:@"Twitter is not configured in Settings."] show];
        //NSLog(@"Not Configured in Settings......"); // show user an alert view that Twitter is not configured in settings.
    }
}
-(IBAction)ForgotPasswordAction:(id)sender{
    
}
-(IBAction)sKipAction:(id)sender{
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"CustomerID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    GridViewController *gridObj=[[GridViewController alloc] init];
    [self presentViewController:gridObj animated:YES completion:nil];
}
#pragma mark UITextfieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
@end
