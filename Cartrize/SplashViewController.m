//
//  SplashViewController.m
//  IShop
//
//  Created by Hashim on 4/30/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "SplashViewController.h"
//#import "GridViewController.h"
#import "ViewController.h"
//#import "ParentViewController.h"
//#import "SWRevealViewController.h"
//#import "WomancategoryView.h"

@interface SplashViewController ()

@end

@implementation SplashViewController
@synthesize navigationController,viewController,loginObj,gridObj;//revealView=_revealView,navigationController;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    
    [activityIndicator startAnimating];

    [self performSelector:@selector(goToRootViewController) withObject:nil afterDelay:3.0];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-Go for nextView
-(void)goToRootViewController{
    self.viewController=[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    self.loginObj=[[LoginView alloc] initWithNibName:@"LoginView" bundle:nil];
    self.gridObj=[[GridViewController alloc] initWithNibName:@"GridViewController" bundle:nil];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"FirstTime"] isEqualToString:@"No"])
    {
        NSString *str=[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomerID"];
        if (str.length==0) {
            [self presentViewController:self.loginObj animated:YES completion:nil];
        }else{
            [self presentViewController:self.gridObj animated:YES completion:nil];

        }
    }
    else
    {
        [self presentViewController:self.viewController animated:YES completion:nil];

    }

}
/*
-(void)goToRootViewController
{
    GridViewController *gridView = [[GridViewController alloc]init];
    ViewController *viewController = [[ViewController alloc] init];
	
    WomancategoryView *listViewController2 = [[WomancategoryView alloc] init];
	
    NSArray *viewControllers = @[viewController, listViewController2];
	
    ParentViewController *tabBarController = [[ParentViewController alloc] init];
	
    tabBarController.delegate = self;
	tabBarController.viewControllers = viewControllers;
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:gridView];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:tabBarController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    mainRevealController.delegate = self;
	self.revealView = mainRevealController;
    [activityIndicator stopAnimating];
    [self presentViewController:self.revealView animated:YES completion:nil];
}

- (NSString*)stringFromFrontViewPosition:(FrontViewPosition)position
{
    NSString *str = nil;
    if ( position == FrontViewPositionLeft ) str = @"FrontViewPositionLeft";
    if ( position == FrontViewPositionRight ) str = @"FrontViewPositionRight";
    if ( position == FrontViewPositionRightMost ) str = @"FrontViewPositionRightMost";
    if ( position == FrontViewPositionRightMostRemoved ) str = @"FrontViewPositionRightMostRemoved";
    return str;
}


- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), [self stringFromFrontViewPosition:position]);
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), [self stringFromFrontViewPosition:position]);
}

- (void)revealController:(SWRevealViewController *)revealController willRevealRearViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didRevealRearViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController willHideRearViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didHideRearViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController willShowFrontViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didShowFrontViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController willHideFrontViewController:(UIViewController *)rearViewController
{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didHideFrontViewController:(UIViewController *)rearViewController

{
    NSLog( @"%@", NSStringFromSelector(_cmd));
}
*/

@end
