//
//  GridViewController.m
//  Cartrize
//
//  Created by Admin on 21/07/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "GridViewController.h"
#import "LoginView.h"
#import "MySingletonClass.h"
#import "CartrizeWebservices.h"
#import "JSON.h"
#import "UIImageView+WebCache.h"
//#import "GridCollctionCell.h"
#import "AFTableViewCell.h"
@interface GridViewController ()

@end

@implementation GridViewController
@synthesize contentOffsetDictionary;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    scroll.backgroundColor=[UIColor clearColor];
    self.contentOffsetDictionary = [NSMutableDictionary dictionary];

    //[colViewObj registerNib:[UINib nibWithNibName:@"GridCollctionCell" bundle:nil] forCellWithReuseIdentifier:@"cellID"];
    activity1.hidden=NO;
    [activity1 startAnimating];
    [activity startAnimating];
    activity.hidden=NO;
    bannerArr=[[NSMutableArray alloc] init];
    categoriesArr=[[NSMutableArray alloc] init];
    catProductList=[[NSMutableArray alloc] init];
    [self getTheImagesFromWebServer];
    [self getAllCategories];
    count=0;
}
-(void)changeBanner{
    [scroll setPagingEnabled:YES];
    for (int i=0; i<bannerArr.count; i++) {
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            UIImageView *img=[[UIImageView alloc] initWithFrame:CGRectMake(768*i, 0, 768, 303)];
            img.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[bannerArr objectAtIndex:i]]];
            //[img setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[bannerImgArr objectAtIndex:i]]] placeholderImage:nil completed:nil];
            [scroll addSubview:img];
            scroll.contentSize=CGSizeMake(768*i+768, 250);
        }
        else{
            if ([UIScreen mainScreen].bounds.size.height==568) {
                UIImageView *img=[[UIImageView alloc] initWithFrame:CGRectMake(320*i, 0, 320, 152)];
                //img.image=myImage;
                [img setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[bannerArr objectAtIndex:i]]] placeholderImage:nil completed:nil];
               // img.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[bannerArr objectAtIndex:i]]];
                [scroll addSubview:img];
                scroll.contentSize=CGSizeMake(320*i+320, 120);
            }else{
                UIImageView *img=[[UIImageView alloc] initWithFrame:CGRectMake(320*i, 0, 320, 132)];
                [img setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[bannerArr objectAtIndex:i]]] placeholderImage:nil completed:nil];
                //img.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[bannerArr objectAtIndex:i]]];
                [scroll addSubview:img];
                scroll.contentSize=CGSizeMake(320*i+320, 120);
            }
        }
    }
    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(scrollPages) userInfo:nil repeats:YES];
    count=0;
}
-(void)scrollPages{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [scroll setContentOffset:CGPointMake(768*count, 0) animated:YES];
    }else
        [scroll setContentOffset:CGPointMake(320*count, 0) animated:YES];
    count++;
    if (count>bannerArr.count-1) {
        count=0;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Webservice calling

-(void)getTheImagesFromWebServer
{
    NSString *jsonString=[NSString stringWithFormat:@"getSpleshScreen"];
    NSLog(@"this is json string%@",jsonString);
    
    [[MySingletonClass sharedSingleton] getDataFromJson:jsonString getData:^(NSArray *data,NSError *error)
     {
         if (error)
         {
         }
         else
         {
             NSLog(@"this is data%@",data);
             bannerArr=[data valueForKey:@"url"];
             [self changeBanner];
             [activity stopAnimating];
             activity.hidden=YES;
         }
     }];
}
-(void)getAllCategories{
    [CartrizeWebservices PostMethodWithApiMethod:@"getAllCategories" Withparms:nil WithSuccess:^(id response)
     {
         //NSLog(@"Response = %@",[response JSONValue]);
         categoriesArr=[response JSONValue];
         [self getCategoryProducts];
         
     } failure:^(NSError *error)
     {
         NSLog(@"Error =%@",[error description]);
     }];

}

-(void)getCategoryProducts{
    for (int i=0; i<categoriesArr.count; i++) {
        NSDictionary *parameters = @{@"customer_id":[[categoriesArr valueForKey:@"category_id"] objectAtIndex:i]};
        [CartrizeWebservices PostMethodWithApiMethod:@"getProductsByCatId" Withparms:parameters WithSuccess:^(id response)
         {
             NSLog(@"Response = %@",[response JSONValue]);
             [catProductList addObject:[response JSONValue]];
             if (i==categoriesArr.count-1) {
                 [self getArray];
             }
         } failure:^(NSError *error)
         {
             NSLog(@"Error =%@",[error description]);
         }];
    }
    
}
-(void)getArray{
    NSLog(@"product List %@",catProductList);
    [productList reloadData];
    [colViewObj reloadData];
    [activity1 stopAnimating];
    activity1.hidden=YES;
}


#pragma mark- IBActions
-(IBAction)logoutAction:(id)sender{
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"CustomerID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    LoginView *gridObj=[[LoginView alloc] init];
    [self presentViewController:gridObj animated:YES completion:nil];
}
//#pragma mark- UICollectionViewDatasource
//-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
//    return categoriesArr.count;
//}
//-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    NSArray *arr=[catProductList objectAtIndex:section];
//    return arr.count;
//}
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *cellId=@"cellID";
//    GridCollctionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
//    NSArray *arr=[catProductList objectAtIndex:indexPath.section];
//
//    cell.lbl.text=[[arr valueForKey:@"prd_name"] objectAtIndex:indexPath.row];
//    return cell;
//
//}
//-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
//          viewForSupplementaryElementOfKind:(NSString *)kind
//                                atIndexPath:(NSIndexPath *)indexPath
//{
//    UICollectionReusableView *headerView;
//    if (kind == UICollectionElementKindSectionHeader) {
//        headerView = [colViewObj dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"cellID" forIndexPath:indexPath];
//        
//        UIView * view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 80)];
//        view.backgroundColor = [UIColor redColor];
//        
//        [headerView addSubview:view];
//        
//        //return headerView;
//    }
//    return headerView;
//}
#pragma mark - UITableViewDelegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSArray *arr=[catProductList objectAtIndex:section];
        return arr.count;
   // return 10;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arr=[catProductList objectAtIndex:indexPath.section];

    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    UIImageView *imgBg=[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 90, 90)];
    imgBg.image=[UIImage imageNamed:@"bg_cateimg.png"];
        UIImageView * img=[[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 80, 80)];
    [imgBg addSubview:img];
        UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(10, 90, 90, 50)];
    lbl.textAlignment=NSTextAlignmentCenter;
    lbl.font=[UIFont fontWithName:@"Helvetica" size:14];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[arr valueForKey:@"prd_thumb"] objectAtIndex:indexPath.row]]];
            dispatch_async(dispatch_get_main_queue(), ^{
                //img.image=[UIImage imageNamed:[[arr valueForKey:@"prd_thumb"] objectAtIndex:indexPath.row]];
                img.image=[UIImage imageNamed:@""];
                img.image=[UIImage imageWithData:data1];
            });
        });
    lbl.text=@"";
    lbl.text=[[arr valueForKey:@"prd_name"] objectAtIndex:indexPath.row];
        [cell.contentView addSubview:lbl];
        [cell.contentView addSubview:imgBg];
   // NSArray *collectionViewArray = self.colorArray[collectionView.tag];
   // cell.backgroundColor = [UIColor redColor];
    
    return cell;
}

#pragma mark UITableViewDatasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return categoriesArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *arr=[catProductList objectAtIndex:section];
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    AFTableViewCell *cell = (AFTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[AFTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(AFTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];

    [cell setCollectionViewDataSourceDelegate:self index:indexPath.row];
    NSInteger index = cell.collectionView.tag;
    
    CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
    [cell.collectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [[categoriesArr valueForKey:@"category_name"] objectAtIndex:section];
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 27)];
    UIImageView *img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 27)];
    img.image=[UIImage imageNamed:@"bg_subhead_arrow.png"];
    [headerview addSubview:img];
    
    UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(20, 0, 320, 27)];
    lbl.text=[[categoriesArr valueForKey:@"category_name"] objectAtIndex:section];
    lbl.font=[UIFont fontWithName:@"Helvetica" size:13];
    lbl.textColor=[UIColor whiteColor];
    [img addSubview:lbl];
    
    return headerview;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 27;
}
#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (![scrollView isKindOfClass:[UICollectionView class]]) return;
    
    CGFloat horizontalOffset = scrollView.contentOffset.x;
    
    UICollectionView *collectionView = (UICollectionView *)scrollView;
    NSInteger index = collectionView.tag;
    self.contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
}
@end
