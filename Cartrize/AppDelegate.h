//
//  AppDelegate.h
//  Cartrize
//
//  Created by Admin on 19/07/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplashViewController.h"
@class ViewController;
@class LoginView;
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong,nonatomic) SplashViewController *splash;
@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic)ViewController *viewController;
@property(strong,nonatomic)LoginView *loginObj;

@end
